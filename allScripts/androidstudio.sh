#!/bin/bash

#INSTALAÇÃO DO Android Studio
echo "Android Studio installation"
read -p "Do you want to update system? (y/n): " update
if [ "$update" == "y" ]; then
    echo "Updatting system..."
    sleep 3
    sudo apt update
    echo
fi
echo "Installing kvm and bridge-utils..."
sleep 2
sudo apt install -y qemu-kvm bridge-utils wget unzip
echo

echo "Setting user..."
sleep 2
sudo usermod -aG kvm $USER
echo

echo "Installing Android Studio..."
sleep 2
sudo snap install android-studio --classic
echo

echo "Your Android Studio version:"
sudo snap list android-studio
sleep 3
echo

echo "Starting Android Studio..."
sleep 2
android-studio
echo