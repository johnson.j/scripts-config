#!/bin/bash

#INSTALAÇÃO DO AWS CLI
echo "AWS CLI installation"
read -p "Do you want to update system? (y/n): " update
if [ "$update" == "y" ]; then
    echo "Updatting system..."
    sleep 3
    sudo apt update
    echo
fi
echo "Installing awscli in your system..."
sleep 2
sudo apt install -y awscli
echo

echo "Your AWS version:"
sudo aws --version
sleep 3
echo