#!/bin/bash

# Default installation
echo "Default installation"
echo "Set sudo password for $USER"
sudo apt update && sudo apt upgrade -y
echo

# INSTALAÇÃO DO vim, cURL, wget, iputils-ping, traceroute and net-tools
echo "Installing vim, cURL, wget and iputils-ping and net-tools..."
sleep 3
sudo apt install -y vim curl wget iputils-ping net-tools traceroute vim-gtk3 vim-athena
echo

## Git installation
echo "Git installation"
read -p "Do you want to install? (y/n): " s1

if [ "$s1" == "y"  ]; then   
    ./git.sh  
fi

## NVM and NodeJS installation
echo "NVM and NodeJS installation"
read -p "Do you want to install? (y/n): " s2

if [ "$s2" == "y"  ]; then
    ./nvm-nodejs.sh    
fi

## VS Code installation
echo "VS Code installation"
read -p "Do you want to install? (y/n): " s3

if [ "$s3" == "y"  ]; then
    ./vscode.sh    
fi

## Terraform installation
echo "Terraform installation"
read -p "Do you want to install? (y/n): " s4

if [ "$4" == "y" ]; then
    ./terraform.sh
fi

## Docker installation
echo "Docker installation"
read -p "Do you want to install? (y/n): " s5

if [ "$s5" == "y"  ]; then
    ./docker.sh    
fi

## Docker compose installation
echo "Docker compose installation"
read -p "Do you want to install? (y/n): " s6

if [ "$s6" == "y"  ]; then
    ./docker-compose.sh    
fi

## AWS CLI installation
echo "AWS CLI installation"
read -p "Do you want to install? (y/n): " s7

if [ "$s7" == "y"  ]; then
    ./awscli.sh    
fi

## VirtualBox installation
echo "VirtuaBox 6 installation"
read -p "Do you want to install? (y/n): " s8

if [ "$s8" == "y"  ]; then
    ./virtualbox.sh    
fi

## JDK8 installation
echo "JDK* installation"
read -p "Do you want to install? (y/n): " s9

if [ "$s9" == "y"  ]; then
    ./openjdk8.sh    
fi

## NetBeans IDE installation
echo "NetBeans IDE installation"
read -p "Do you want to install? (y/n): " s10

if [ "$s10" == "y"  ]; then
    ./netbeans.sh    
fi

## Android Studio IDE installation
echo "Android Studio IDE installation"
read -p "Do you want to install? (y/n): " s11

if [ "$s11" == "y"  ]; then
    ./androidstudio.sh    
fi

## Opera Browser installation
echo "Opera Browser IDE installation"
read -p "Do you want to install? (y/n): " s12

if [ "$s12" == "y"  ]; then
    ./operabrowser.sh    
fi

## Spotify installation
echo "Spotify installation"
read -p "Do you want to install? (y/n): " s13

if [ "$s13" == "y"  ]; then
    ./spotify.sh    
fi

echo "Finished"
echo
echo "Close and reopen your terminal to apply!"
echo
echo "Good Job!!!"
echo
echo "Developed by DevPresto :-)"
