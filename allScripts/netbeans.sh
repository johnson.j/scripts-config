#!/bin/bash

#INSTALAÇÃO DO NetBeans IDE
echo "NetBeans IDE installation"
read -p "Do you want to update system? (y/n): " update
if [ "$update" == "y" ]; then
    echo "Updatting system..."
    sleep 3
    sudo apt update
    echo
fi
echo "Download a Netbeans package from the terminal ..."
sleep 2
wget -c http://download.netbeans.org/netbeans/8.2/final/bundles/netbeans-8.2-linux.sh
echo

echo "Applying and executing permissions..."
sleep 1
chmod +x netbeans-8.2-linux.sh
echo

echo "Installing NetBeans IDE..."
sleep 2
./netbeans-8.2-linux.sh
echo