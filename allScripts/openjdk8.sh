#!/bin/bash

#INSTALAÇÃO DO JDK8
echo "zulu-openjdk8 installation"
read -p "Do you want to update system? (y/n): " update
if [ "$update" == "y" ]; then
    echo "Updatting system..."
    sleep 3
    sudo apt update
    echo
fi
echo "Download zulu-openjdk8..."
sleep 2
wget -c https://cdn.azul.com/zulu/bin/zulu8.38.0.13-ca-jdk8.0.212-linux_amd64.deb

echo "Installing zulu-openjdk8..."
sudo ~/Downloads/dpkg -i zulu8.38.0.13-ca-jdk8.0.212-linux_amd64.deb

echo "Copy zulu-openjdk8 to jvm..."
sudo ~/Downloads/cp -r zulu-8-amd64 /usr/lib/jvm/
echo

echo "Your java version:"
sudo java -version
sleep 3
echo

echo "Set the JAVA_HOME environment variable"
echo "Use: 'sudo vim /etc/environment'"
echo 'Set JAVA_HOME="/usr/lib/jvm/java-11-openjdk-amd64"'
echo
