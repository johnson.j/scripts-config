#!/bin/bash

#INSTALAÇÃO DO Opera Browser
echo "Opera Browser installation"
read -p "Do you want to update system? (y/n): " update
if [ "$update" == "y" ]; then
    echo "Updatting system..."
    sleep 3
    sudo apt update
    echo
fi
echo "Adding an Opera repository and keyring..."
sleep 2
wget -qO- https://deb.opera.com/archive.key | sudo apt-key add -
sudo add-apt-repository "deb [arch=i386,amd64] https://deb.opera.com/opera-stable/ stable non-free"
echo

echo "Installing opera browser..."
sleep 2
sudo apt install -y opera-stable
echo