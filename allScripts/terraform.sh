#!/bin/bash

# Instalação do terraform
echo "Terraform installation"
read -p "Do you want update your system? (y/n): " update
if [ "$update" == "y" ]; then
	echo "Updatting system..."
	sleep 3
	sudo apt update
	echo
fi
echo "Finding for unzip!!!"
sleep 2
if [ ! unzip -h 2&> /dev/null ]; then
	echo "Aborting - unzip not installed and required"
	exit 1
fi
echo "Downloading terraform..."
sleep 2
wget https://releases.hashicorp.com/terraform/0.11.13/terraform_0.11.13_linux_amd64.zip
echo "Unzip terraform..."
sleep 2
unzip ./terraform_*.zip
echo "moving terraform to /usr/local/bin/terraform..."
sleep 3
sudo mv ./terraform /usr/local/bin/terraform
echo
echo "Your terraform version"
sleep 3
terraform --version
echo
