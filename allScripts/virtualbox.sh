#!/bin/bash

#INSTALAÇÃO DO VirtualBox
echo "VirtualBox 6 installation"
read -p "Do you want to update system? (y/n): " update
if [ "$update" == "y" ]; then
    echo "Updatting system..."
    sleep 3
    sudo apt update
    echo
fi
echo "Importing the GPG keys..."
sleep 2
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
echo

echo "Adding the VirtualBox repository..."
sleep 2
sudo add-apt-repository "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian bionic contrib"
echo

echo "Updatting your system..."
sleep 2
sudo apt update
echo

echo "Installing virtuallbox 6..."
sleep 2
sudo apt install -y virtualbox-6.0
echo